<div class="d-flex flex-column flex-md-row align-items-center p-1 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">PHP Blog</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="/">Главная</a>
        <a class="p-2 text-dark" href="contacts.php">Контакты</a>
        <?php
            if($_COOKIE['log'] != '')
            echo '<a class="p-2 text-dark" href="article.php">Добавить статью</a>';
        ?>
    </nav>
    <?php
        if($_COOKIE['log'] == ''):
    ?>
    <a class="btn btn-outline-primary m-2" href="auth.php">Войти</a>
    <a class="btn btn-outline-primary" href="reg.php">Регистрация</a>
    <?php
        else:
    ?>
    <a class="btn btn-outline-primary mb-2" href="auth.php">Кабинет пользователя</a>
    <?php
        endif;
    ?>
</div>