<aside class="col-md-4">
    <div class="p-3 mb-3 bg-warning rounded">
        <?php
        require_once 'mysql_connect.php';
        $sql = 'SELECT * FROM `articles` ORDER BY `date` DESC';
        $query = $pdo->query($sql);
        while($row = $query->fetch(PDO::FETCH_OBJ)) {
            echo
            "<div class='mb-2'>
                <h4>$row->title</h4>
                <p>$row->intro</p>
                <a href='news.php?id=$row->id' title='$row->title'>
                   Прочитать больше
                </a>
            </div>";
        }
        ?>
    </div>
<!--    <div class="p-3 mb-3">-->
<!--        <img src="https://itproger.com/img/courses/1534230100.jpg" alt="" class="img-thumbnail">-->
<!--    </div>-->
</aside>