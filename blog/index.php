<!doctype html>
<html lang="en">
<head>
    <?php
        $website_title = 'PHP Blog';
        require 'components/head.php';
    ?>
</head>
<body>

    <?php require 'components/header.php'?>

    <main class="container mt-5">
        <div class="row">
            <div class="col-md-8 mb-3">
                <?php
                    require_once 'mysql_connect.php';
                    $sql = 'SELECT * FROM `articles` ORDER BY `date` DESC';
                    $query = $pdo->query($sql);
                    while($row = $query->fetch(PDO::FETCH_OBJ)) {
                        echo
                        "<div class='mb-5'>
                            <h2>$row->title</h2>
                            <p>$row->intro</p>
                            <p><b>Автор статьи:</b> <mark>$row->author</mark></p>
                            <a href='news.php?id=$row->id' title='$row->title'>
                                <button class='btn btn-warning mb-2'>Прочитать больше</button>
                            </a>
                        </div>";
                    }
                ?>
            </div>
            <?php require 'components/aside.php'?>
        </div>
    </main>

    <?php require 'components/footer.php'?>

</body>
</html>
