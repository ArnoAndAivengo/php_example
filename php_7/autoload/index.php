<?php


function autoloader($class)
{
    $file = __DIR__ . "/classes/{$class}.php";
    if (file_exists($file)) {
        require_once $file;
    }
}

spl_autoload_register('autoloader');