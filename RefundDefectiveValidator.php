<?php


namespace App\Validators;

/**
 * Class RefundDefectiveValidator
 *
 * @package App\Validators
 */
class RefundDefectiveValidator
{
    protected $result = [];

    /**
     * @param array $data
     *
     * @param string $deliveryNumber
     *
     * @return bool
     */
    public function preValidate(array $data, $deliveryNumber)
    {
        if (!isset($data['id']) || !isset($data['count'])) {
            $this->result['success'] = false;
            $this->result['message'] = 'Переданы некорректные данные';

            return false;
        }

        if (empty($deliveryNumber)) {
            $this->result['success'] = false;
            $this->result['message'] = 'Не указан номер транспортной накладной';

            return false;
        }

        return true;
    }

    /**
     * @param \App\Db\DefectiveRow $model
     * @param int $count
     * @param int $availableCount
     * @param int $i
     *
     * @return bool
     */
    public function validateDefective($model, $count, $availableCount, $i)
    {
        if ($model === null) {
            $this->result['success'] = false;
            $this->result['message'] = 'ЗИП в строке ' . ($i + 1) . ' не найден';

            return false;
        }

        $zip = $model->zip();

        if ((int)$model->count < (int)$count) {
            $this->result['success'] = false;
            $this->result['message'] = "По позиции $zip->article $zip->name можно вернуть запчастей не более $model->count шт";

            return false;
        } elseif ((int)$count <= 0) {
            $this->result['success'] = false;
            $this->result['message'] = "По позиции $zip->article $zip->name можно вернуть запчастей не менее 1шт";

            return false;
        }

        if ((int)$count > (int)$availableCount) {

            $this->result['success'] = false;
            $this->result['message'] = "По позиции $zip->article $zip->name возврат не может быть проведен, т.к. позиция находится на восстановлении в Ремонтном центре";

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
