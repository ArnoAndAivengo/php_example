<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <?php

        class User {

            const LOGIN = 'AKKA';

            public $name = 'John';
            private $surname = 'Smith';
            private $email = 'johnSmith@matirx.net';
            private $login;
            private $pass;

            public function __construct($name, $surname, $login)
            {
                $this->name = $name;
                $this->surname = $surname;
                $this->login = $login;
                
            }

            public function __destruct()
            {
                print 'Destruct '.__CLASS__;
            }
            /**
             * @return string
             */
            public function getName(): string
            {
                return $this->name;
            }

            /**
             * @param string $name
             */
            public function setName(string $name): void
            {
                $this->name = $name;
            }

            /**
             * @return mixed
             */
            public function getLogin()
            {
                return $this->login;
            }

        }
        echo User::LOGIN;

        $admin = new User('ALex', 'Obuhov', 'Akka');






    ?>

</body>
</html>