<?php

//Interface

//    interface Human {
//        public function talk();
//        public function walk();
//    }
//
//    interface Mutant extends Human {
//
//        public function fly();
//
//    }
//
//    class Person implements Human, Mutant {
//
//
//        public function talk() {
//            echo 'Go GO Go'.'</br>';
//        }
//        public function walk(){
//            echo 'Go G+O Go' . '</br>';
//        }
//
//        public function fly() {
//            echo 'Fly Mutant';
//        }
//
//    }
//
//$bb = new Person();
//
//    $bb->talk();
//    $bb->walk();
//    $bb->fly();


// traits

//trait PrintSome {
//    public function talk($text) {
//        echo $text;
//    }
//}
//
//
//class Test {
//    use PrintSome;
//}
//
//$obj = new Test();
//$obj->talk('Hello My Friends!');


// abstracts class

abstract class Car {

    protected $speed;
    protected $color;

    abstract protected function showInfo();

}


class Audi extends Car {

    public function __construct($speed)
    {
        $this->speed = $speed;
    }


    public function showInfo() {
        echo "Скорость Автомобиля: ". $this->speed;
    }
}


$audi = new Audi(400);
$audi->showInfo();



