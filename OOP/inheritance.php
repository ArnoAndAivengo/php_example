<?php

    class Car {

        protected $speed;
        protected $wheels;
        protected $colors;

        public function __construct($speed, $colors)
        {
            $this->speed = $speed;
            $this->colors = $colors;
        }


        function showInfo() {
            echo "Скорость автомбиля: " .$this->speed.'</br>';
        }
    }


    class Audi extends Car {

        private $model;

         function __construct($speed, $colors, $model) {
            parent::__construct($speed, $colors);
            $this->model = $model;
        }

        /**
         * @param mixed $model
         */
        public function setModel(): void
        {
            echo "Модель Автомобиля: ".$this->model.'</br>';
            echo "Цвет Автомобиля: ".$this->colors.'</br>';
        }

    }


    $m3 = new Audi(340, "White", "TT");
    $m3->showSpeed();
    $m3->setModel();

